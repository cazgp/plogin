# plogin

This is a set of tables and functions based around Postgres RLS, hopefully allowing us to use the framework to its fullest extent.

The idea is that each user in the system is given their own Postgres role, which is managed by administrators, and various permissions that can be managed by a developer.

The `account` table is designed to be deliberately small. Adding additional information will require migrations in the application using this as a foundation.

This is just a framework. Every decision made here can be changed by migrations. It is merely a starting point to facilitate this type of user authentication in Postgres. It is not designed to provide for every use-case.

## Scripts

To test:

Note that VERSION can be 16 (since 16 the way that roles work has changed drastically and so this is not tested for any other version).

```
# First run
DO_BUILD=1 VERSION=16 ./test.bash

# Consecutive runs
./test.bash
```

## Permissions

Important things to know:

- There is a user `plogin_user_1` who is created when the database is created. This user is an uber user, as allows us to bootstrap the application. This role can never be deleted.
- `CREATEROLE` is not inheritable. This means that even though we create a role `plogin_perms_admin` which has that permission, anything inheriting will not get it. We therefore need to handle adding and removing this privilege separately.
- Since version 16, the way that roles and permissions work in Postgres has changed. It used to be that a user with `CREATEROLE` privileges could `GRANT` any role. That has now changed, and to do anything with roles requires the `WITH ADMIN TRUE` option to be set.
- There is an `is_admin` column in the `account` table which we use to selectively apply admin rights. It is a design decision that adding/removing roles is something that only an admin can do, and will only ever be something that an admin can do. It's too complicated to delegate role setting like that, and it's hard to think of a use-case out-the-box where something super complicated would be required.

## Schema

### Tables

TODO

### Functions

TODO

## Develepment

### Testing

## Deployment
