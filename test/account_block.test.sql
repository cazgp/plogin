BEGIN;

SELECT plan (19);

-- Create a user to block.
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'Blocked', 'blocked@example.com', 'passwordpass'
      $$
    , 'Create blocked user'
    );

SELECT LIVES_OK
    ( $$ SELECT plogin.role_add('plogin_user_' || id, 'plogin_perms_test', TRUE)
           FROM plogin.account
          WHERE email = 'blocked@example.com'
      $$
    , 'configure roles'
    );

SELECT IS_MEMBER_OF('plogin_perms_test', 'plogin_user_' || (SELECT blocked_id_get('plogin')));


--
-- account block fails when
-- user doesn't have permission to block
--

-- Set the current role to the user to block.
SET ROLE none;
SELECT tap.role_set('blocked@example.com');

-- Ensure that updating fails
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET blocked_at = NOW()
        WHERE email = 'blocked@example.com'
    $$
  , 'permission denied for table account'
  );


-- Ensure that 'blocked_at' and 'blocked_by' are still null
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT blocked_at, blocked_by
         FROM tap.account
        WHERE email = 'blocked@example.com'
    $$
  , $$ SELECT NULL::TIMESTAMPTZ, NULL::INT $$
  , 'blocked_at and blocked_by are null'
  );

-- Ensure the role still exists
SELECT LIVES_OK
  ( $$ SELECT tap.role_set('blocked@example.com') $$
  , 'role still exists'
  );

--
-- account block fails when
-- user doesn't have permission to block
--

SET ROLE plogin_user_1;

-- Ensure that updating fails
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET blocked_at = NOW()
        WHERE id = 1
    $$
  , 'Cannot block self'
  );


-- Ensure that 'blocked_at' and 'blocked_by' are still null
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT blocked_at, blocked_by
         FROM tap.account
        WHERE email = 'blocked@example.com'
    $$
  , $$ SELECT NULL::TIMESTAMPTZ, NULL::INT $$
  , 'blocked_at and blocked_by are null'
  );

-- Ensure the role still exists
SELECT LIVES_OK
  ( $$ SELECT tap.role_set('blocked@example.com') $$
  , 'role still exists'
  );

--
-- account block succeeds when
-- the user has permission
--
SET ROLE plogin_user_1;

SELECT LIVES_OK
  ( $$ UPDATE plogin.account
          SET blocked_at = NOW() - INTERVAL '1 hour'
        WHERE email = 'blocked@example.com'
    $$
  , 'blocking user works'
  );

-- Ensure the blocked fields are correct
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT blocked_at, blocked_by
         FROM tap.account
        WHERE email = 'blocked@example.com'
    $$
  , $$ SELECT NOW(), 1 $$
  , 'blocked_at and blocked_by are null'
  );


-- Ensure we can no longer set the role
SELECT THROWS_OK
  ( $$ SELECT tap.role_set('blocked@example.com') $$
  , 'permission denied to set role "plogin_user_' || (SELECT blocked_id_get('tap')) || '"'
  );


-- Ensure that the correct audit log has been created
SELECT RESULTS_EQ
  ( $$ SELECT type, at, by, who, account
         FROM tap.account_audit
    $$
  , $$ SELECT 'blocked'::plogin.account_audit_type
            , NOW()
            , 1
            , (SELECT blocked_id_get('tap'))
            , NULL::plogin.account_audit_user
    $$
  , 'ensure audit logs exist'
  );


--
-- account block fails when
-- the user is already blocked
--

-- Set the blocked_at and blocked_by to something else than what it ought to be.
SET ROLE none;
SELECT tap.trigger_disable();
SET ROLE plogin_user_1;
SELECT RESULTS_EQ
  ( $$ UPDATE plogin.account
          SET blocked_at = NOW() - INTERVAL '1 hour'
            , blocked_by = (SELECT blocked_id_get('plogin'))
        WHERE email = 'blocked@example.com'
        RETURNING blocked_at, blocked_by
    $$
  , $$ SELECT NOW() - INTERVAL '1 hour', (SELECT blocked_id_get('plogin')) $$
  , 'resetting blocked time'
  );
SELECT tap.trigger_enable();

-- Block again.
SET ROLE plogin_user_1;
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET blocked_at = NOW() - INTERVAL '1 hour'
        WHERE email = 'blocked@example.com'
    $$
  , 'Cannot block an already blocked account'
  );


-- Ensure nothing has changed.
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT blocked_at, blocked_by
         FROM tap.account
        WHERE email = 'blocked@example.com'
    $$
  , $$ SELECT NOW() - INTERVAL '1 hour', (SELECT blocked_id_get('tap')) $$
  , 'blocked_at and blocked_by are correct'
  );


-- Unblock the user and ensure they exist again correctly.
SET ROLE plogin_user_1;
SELECT LIVES_OK
  ( $$ UPDATE plogin.account
          SET blocked_at = NULL
        WHERE email = 'blocked@example.com'
    $$
  , 'unblocking account'
  );

-- Ensure the role exists again.
SET ROLE none;
SELECT LIVES_OK
  ( $$ SELECT tap.role_set('blocked@example.com') $$
  , 'could not set role'
  );

SELECT IS_MEMBER_OF('plogin_perms_test', 'plogin_user_' || (SELECT blocked_id_get('plogin')));


 -----------------------
 SELECT * FROM finish();
 ROLLBACK;
