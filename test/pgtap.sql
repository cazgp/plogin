CREATE SCHEMA tap;
SET search_path=tap;
CREATE EXTENSION IF NOT EXISTS pgtap;

-- Create a blank permission to use in testing.
CREATE ROLE plogin_perms_test;
GRANT plogin_perms_test TO plogin_user_1 WITH ADMIN TRUE;

-- Create a bunch of useful views.
CREATE VIEW tap.account_session AS SELECT * FROM plogin.account_session;
GRANT SELECT ON tap.account_session TO plogin;
GRANT UPDATE ON tap.account_session TO plogin;

CREATE VIEW tap.account_audit AS SELECT * FROM plogin.account_audit;
CREATE VIEW tap.login_audit AS SELECT * FROM plogin.login_audit;
GRANT SELECT ON tap.login_audit TO plogin;
GRANT SELECT ON tap.account_audit TO plogin;

CREATE VIEW tap.account AS SELECT * FROM plogin.account;
GRANT SELECT ON tap.account TO PUBLIC;

CREATE FUNCTION tap.role_set(_email plogin.CITEXT)
RETURNS VOID
LANGUAGE plpgsql
AS $OUTER$
DECLARE
  _account_id INT;
BEGIN

  SELECT tap.account.id INTO _account_id
    FROM tap.account
   WHERE tap.account.email = _email;

  EXECUTE 'SET ROLE plogin_user_' || _account_id;

END;
$OUTER$;


CREATE FUNCTION tap.assert_no_login(_email plogin.CITEXT, _password TEXT)
RETURNS SETOF TEXT
LANGUAGE plpgsql
AS $OUTER$
BEGIN
  -- Ensure that trying to login returns nothing
  RETURN NEXT IS_EMPTY
    ( $$ SELECT account_id, session_token
           FROM plogin.account_login
                ( '$$ || _email || $$'
                , '$$ || _password || $$'
                )
      $$
    , _email || ' account_login should fail'
    );

  -- Ensure nothing was added to the sessions table for that account
  RETURN NEXT IS
    ( COUNT(*)::int
    , 0
    , 'Sessions should not be created'
    )
  FROM tap.account_session
  JOIN tap.account
    ON tap.account.id = tap.account_session.account
 WHERE tap.account.email = _email;

  -- Ensure the failed login attempt is recorded
  RETURN NEXT RESULTS_EQ
      ( $$ SELECT type, email::TEXT, at FROM tap.login_audit ORDER BY id DESC LIMIT 1 $$
      , $$ SELECT 'failed'::login_audit_type, '$$ || _email || $$' , NOW() $$
      , 'Failed login not recorded'
      );

  -- Ensure the current user hasn't changed
  RETURN NEXT tap.assert_current_user('plogin');

END;
$OUTER$;

CREATE FUNCTION tap.assert_login(_email plogin.CITEXT, _password TEXT, _relog BOOL)
RETURNS SETOF TEXT
LANGUAGE plpgsql
AS $OUTER$
DECLARE
  _account_id INT;
BEGIN
  -- Ensure nothing is in the sessions table for that account
  RETURN NEXT IS
    ( COUNT(*)::INT
    , _relog::INT
    , _email || ' should have ' || _relog::INT || ' sessions'
    )
  FROM tap.account_session
  JOIN tap.account
    ON tap.account.id = tap.account_session.account
 WHERE tap.account.email = _email;

  -- Ensure that logging-in succeeds
  RETURN NEXT LIVES_OK
    ( $$ SELECT account_id, session_token
           FROM plogin.account_login
                ( '$$ || _email || $$'
                , '$$ || _password || $$'
                )
      $$
    , _email || ' account_login should succeed'
    );

  -- Ensure something was added to the sessions table for that account
  RETURN NEXT IS
    ( expires_at
    , NOW() + INTERVAL '1 hour'
    , 'Sessions should be created'
    )
  FROM tap.account_session
  JOIN tap.account
    ON tap.account.id = tap.account_session.account
 WHERE tap.account.email = _email;

  -- Ensure the successful login is recorded
  RETURN NEXT RESULTS_EQ
      ( $$ SELECT type, email, at FROM tap.login_audit ORDER BY id DESC LIMIT 1 $$
      , $$ SELECT 'succeeded'::login_audit_type, '$$ || _email || $$'::CITEXT , NOW() $$
      , 'Login recorded'
      );

  -- Ensure the current user hasn't changed
  RETURN NEXT tap.assert_current_user('plogin');

END;
$OUTER$;

-- Helper function to assert the current user.
CREATE FUNCTION tap.assert_current_user(_user TEXT)
RETURNS SETOF TEXT
LANGUAGE plpgsql
AS $OUTER$
BEGIN
  -- For some reason postgres 14 doesn't like CURRENT_USER being cast to TEXT,
  -- it complains with collation issues... but CITEXT is quite happy.
  RETURN NEXT RESULTS_EQ
    ( $$ SELECT CURRENT_USER::CITEXT $$
    , $$ SELECT '$$ || _user || $$'::CITEXT $$
    , 'current user should be ' || _user
    );
END;
$OUTER$;


--- Helper functions to disable triggers when needed.
CREATE FUNCTION tap.trigger_disable()
RETURNS VOID
SECURITY DEFINER
LANGUAGE plpgsql
AS $$
BEGIN
    ALTER TABLE plogin.account DISABLE TRIGGER account_audit_trigger;
    ALTER TABLE plogin.account DISABLE TRIGGER account_trigger;
END;
$$;

CREATE FUNCTION tap.trigger_enable()
RETURNS VOID
SECURITY DEFINER
LANGUAGE plpgsql
AS $$
BEGIN
    ALTER TABLE plogin.account ENABLE TRIGGER account_audit_trigger;
    ALTER TABLE plogin.account ENABLE TRIGGER account_trigger;
END;
$$;

CREATE FUNCTION tap.blocked_id_get(_schema TEXT)
RETURNS TABLE(id INT)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    EXECUTE FORMAT
        ( 'SELECT id FROM %s.account WHERE email = ''blocked@example.com'''
        , _schema
        );
END;
$$;



GRANT ALL ON SCHEMA tap TO public;
GRANT ALL ON SCHEMA tap TO plogin_everyone;

GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA tap TO public;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA tap TO plogin_everyone;
