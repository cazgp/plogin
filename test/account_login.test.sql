BEGIN;

SELECT plan (39);

--
-- account_login fails when
-- attempting to login with an email address that doesn't exist
--
SELECT tap.assert_no_login('doesnotexist'::CITEXT, 'password');


--
-- account_login succeeds when
-- the email and password are correct
--
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'name', 'exists@example.com', 'passwordpass'
      $$
    , 'Create user'
    );

-- Login
SET ROLE plogin;
SELECT tap.assert_login('exists@example.com', 'passwordpass', FALSE);
SELECT tap.role_set('exists@example.com');

-- Logout
SELECT LIVES_OK
  ( $$ SELECT plogin.account_logout() $$
  , 'logout exists@example.com'
  );

--
-- account_login fails when
-- the password is incorrect
--
SELECT tap.assert_no_login('exists@example.com'::CITEXT, 'wrong');

--
-- account_login fails when
-- the admin user tries to do it
--
SELECT THROWS_OK
  ( $$ SET ROLE plogin_user_1;
       SELECT plogin.account_login('anything', 'anything') $$
  , 'permission denied for function account_login'
  , 'account_login should fail for admin user'
  );


--
-- account_login fails when
-- a non-user role tries to do it
--
SELECT THROWS_OK
  ( $$ SET ROLE plogin_everyone;
       SELECT plogin.account_login('anything', 'anything') $$
  , 'permission denied for function account_login'
  , 'account_login fail for plogin_everyone'
  );


--
-- account_login fails when
-- the user is blocked
--

-- Create an account and block it
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'name', 'blocked@example.com', 'passwordpass'
      $$
    , 'Create blocked user'
    );

SELECT RESULTS_EQ
  ( $$ UPDATE plogin.account
          SET blocked_at = NOW()
            , blocked_by = 1
        WHERE plogin.account.id = (SELECT blocked_id_get('plogin'))
       RETURNING 1
    $$
  , $$ SELECT 1 $$
  , 'block blocked@example.com'
  );


SET ROLE plogin;
SELECT tap.assert_no_login('blocked@example.com'::CITEXT, 'passwordpass');

--
-- account_login succeeds when
-- the user is already logged in
--
SELECT tap.assert_login('exists@example.com', 'passwordpass', FALSE);

-- Set the session expires to something else
SELECT RESULTS_EQ
  ( $$ UPDATE tap.account_session
          SET expires_at = '2017-01-01'
         FROM tap.account
        WHERE tap.account.email = 'exists@example.com'
          AND tap.account.id = tap.account_session.account
    RETURNING expires_at
    $$
  , $$ SELECT '2017-01-01'::TIMESTAMPTZ $$
  , 'set expires to something in the past'
  );

-- Login again
SELECT tap.assert_login('exists@example.com', 'passwordpass', TRUE);

--
-- account_login enumeration check
--

-- Create 50 accounts so that we can get some measurable time.
-- Any more than 50 takes ages and it doesn't appear to affect the times in any way.
SET ROLE plogin_user_1;

\o /dev/null
INSERT INTO plogin.account (name, email, password)
SELECT 'Name' || n
     , ('email' || n || '@example.com')::plogin.CITEXT
     , 'passwordpass'
FROM generate_series
  ( (SELECT max(id) + 1  FROM tap.account)
  , (SELECT max(id) + 50 FROM tap.account)
  ) n;
\o

-- All types of login should take the same amount of time.
SET ROLE plogin;
SELECT PERFORMS_WITHIN
  ( $$ SELECT * FROM account_login('exists@example.com', 'passwordpass') $$
  , 11, 2, 10
  , 'correct password login time trial'
  );

SELECT PERFORMS_WITHIN
  ( $$ SELECT * FROM account_login('exists@example.com', 'wrong') $$
  , 11, 2, 10
  , 'incorrect password login time trial'
  );

SELECT PERFORMS_WITHIN
  ( $$ SELECT * FROM account_login('blocked@example.com', 'passwordpass') $$
  , 11, 2, 10
  , 'blocked correct password login time trial'
  );

SELECT PERFORMS_WITHIN
  ( $$ SELECT * FROM account_login('blocked@example.com', 'incorrect') $$
  , 11, 2, 10
  , 'blocked incorrect password login time trial'
  );

SELECT PERFORMS_WITHIN
  ( $$ SELECT * FROM account_login('wrong@example.com', 'wrong') $$
  , 11, 2, 10
  , 'wrong account login time trial'
  );


-----------------------
SELECT * FROM finish();
ROLLBACK;
