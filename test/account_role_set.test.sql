BEGIN;

SELECT plan (9);

-- Create a user
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'name', 'normal@example.com', 'passwordpass'
      $$
    , 'Create normal user'
    );

-- Log in to get a token
SET ROLE none;
SELECT session_token INTO tap._session_token
  FROM account_login('normal@example.com', 'passwordpass');

-- Set the expires date to pretty low
SELECT LIVES_OK
    ( $$ UPDATE account_session
            SET expires_at = NOW() + INTERVAL '1 minute'
      $$
    , 'reset the expiry token'
    );

-- Set the role
SELECT RESULTS_EQ
    ( $$ SELECT id, name
           FROM plogin.account_role_set
              ( ( SELECT * FROM tap._session_token) )
      $$
    , $$ SELECT ( SELECT id FROM account WHERE email = 'normal@example.com' ), 'name' $$
    , 'successfully set role'
    );

-- Ensure the current user has changed
SELECT tap.assert_current_user('plogin_user_' || (SELECT id FROM plogin.account));

-- Ensure the expires has updated
SET ROLE none;
SELECT RESULTS_EQ
    ( $$ SELECT expires_at::TIMESTAMPTZ FROM account_session $$
    , $$ SELECT NOW() + INTERVAL '1 hour' $$
    , 'expires should be reset'
    );

-- Invalidate the expires_at date on the token
SET ROLE none;
SELECT LIVES_OK
    ( $$ UPDATE account_session
            SET expires_at = NOW() - INTERVAL '1 second'
      $$
    , 'reset the expiry token to before NOW'
    );

-- Ensure that the user can no longer login with that token.
SELECT IS_EMPTY
    ( $$ SELECT plogin.account_role_set(( SELECT * FROM tap._session_token )) $$
    , 'expired token should not work'
    );

-- Try setting the role when the user has not logged in
SELECT IS_EMPTY
    ( $$ SELECT plogin.account_role_set(gen_random_uuid()) $$
    , 'user without token should not be able to set role'
    );

-- Ensure that the user is penalised for having the wrong token.
SELECT PERFORMS_WITHIN
    ( $$ SELECT plogin.account_role_set(gen_random_uuid()) $$
    , 1000, 2, 1
    );

-----------------------
SELECT * FROM finish();
ROLLBACK;
