BEGIN;

SELECT plan (20);

-- Create an admin user.
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account ( name, email, password, is_admin )
         SELECT 'name', 'admin@example.com', 'passwordpass', TRUE
      $$
    , 'Create admin user'
    );

-- Create a normal user.
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account ( name, email, password )
         SELECT 'name', 'normal@example.com', 'passwordpass'
      $$
    , 'Create normal user'
    );

-----------
-- role_add
-----------

--
-- role-add fails
-- when a normal user tries to call it
--
SELECT tap.role_set('normal@example.com');
SELECT THROWS_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_1', 'doesnotexist', TRUE ) $$
    , 'permission denied for function role_add'
    );

--
-- role-add fails
-- when called with a non-existent role
--
SELECT tap.role_set('admin@example.com');
SELECT THROWS_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_1', 'doesnotexist', TRUE ) $$
    , 'role "doesnotexist" does not exist'
    );

--
-- role-add fails
-- when granting admin role caller does not have
--
SELECT THROWS_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_' || id , 'plogin_perms_test', TRUE )
           FROM tap.account
          WHERE email = 'normal@example.com'
      $$
    , 'permission denied to grant role "plogin_perms_test"'
    );

--
-- role-add fails
-- when granting non-admin role caller does not have
--
SELECT THROWS_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_' || id , 'plogin_perms_test', FALSE )
           FROM tap.account
          WHERE email = 'normal@example.com'
      $$
    , 'permission denied to grant role "plogin_perms_test"'
    );

--
-- role-add succeeds
-- when granting non-admin permission to another user
--
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_' || id , 'plogin_perms_test', FALSE )
           FROM tap.account
          WHERE email = 'admin@example.com'
      $$
    , 'granted plogin_perms_test to admin'
    );

-- Ensure the grant actually worked
SELECT IS_MEMBER_OF('plogin_perms_test', 'plogin_user_' || id)
  FROM tap.account
 WHERE email = 'admin@example.com';

-- Ensure admin cannot grant newly given role
SELECT tap.role_set('admin@example.com');
SELECT THROWS_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_' || id , 'plogin_perms_test', FALSE )
           FROM tap.account
          WHERE email = 'normal@example.com'
      $$
    , 'permission denied to grant role "plogin_perms_test"'
    , 'non-admin cannot grant newly given role'
    );


--
-- role-add succeeds
-- when granting admin permission to another user
--
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_' || id , 'plogin_perms_test', TRUE )
           FROM tap.account
          WHERE email = 'admin@example.com'
      $$
    , 'granted plogin_perms_test to admin'
    );

--
-- role-add succeeds
-- when user is an admin of the role they're granting
--
SELECT LIVES_OK
    ( $$ SELECT plogin.role_add ( 'plogin_user_' || id , 'plogin_perms_test', FALSE )
           FROM tap.account
          WHERE email = 'normal@example.com'
      $$
    , 'granted plogin_perms_test to normal'
    );


--------------
-- role_remove
--------------

--
-- role_remove succeeds when caller is admin on the role
--
SELECT LIVES_OK
    ( $$ SELECT plogin.role_remove ( 'plogin_user_' || id , 'plogin_perms_test' )
           FROM tap.account
          WHERE email = 'normal@example.com'
      $$
    , 'removed plogin_perms_test from normal'
    );

-- Ensure normal is no longer a member.
SELECT ISNT_MEMBER_OF('plogin_perms_test', 'plogin_user_' || id)
  FROM tap.account
 WHERE email = 'normal@example.com';


---------------
-- admin_toggle
---------------

--
-- admin_toggle fails when toggling uber user
--
SET ROLE plogin_user_1;
SELECT THROWS_OK
    ( $$ SELECT plogin.admin_toggle ( 'plogin_user_1', TRUE ) $$
    , 'Cannot admin toggle uber user'
    );

SELECT THROWS_OK
    ( $$ SELECT plogin.admin_toggle ( 'plogin_user_1', FALSE ) $$
    , 'Cannot admin toggle uber user'
    );

--
-- admin_toggle succeeds when revoking admin user
--
SELECT LIVES_OK
    ( $$ SELECT plogin.admin_toggle ( 'plogin_user_' || id , FALSE )
           FROM tap.account
          WHERE email = 'admin@example.com'
      $$
    , 'revoked admin from admin'
    );

-- Ensure they can no longer create roles
SELECT tap.role_set('admin@example.com');
SELECT THROWS_OK
    ( $$ INSERT INTO plogin.account ( name, email, password )
         SELECT 'name', 'moo@moo.com', 'passwordpass'
      $$
    , 'permission denied for table account'
    , 'admin user can no longer create roles'
    );

--
-- admin_toggle succeeds when reinstating admin user
--
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ SELECT plogin.admin_toggle ( 'plogin_user_' || id , TRUE )
           FROM tap.account
          WHERE email = 'admin@example.com'
      $$
    , 'reinstated admin user'
    );

-- Ensure they can create roles again
SELECT tap.role_set('admin@example.com');
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account ( name, email, password )
         SELECT 'name', 'moo@moo.com', 'passwordpass'
      $$
    , 'admin user can create roles'
    );

-- Ensure normal user cannot do anything
SELECT tap.role_set('normal@example.com');
SELECT THROWS_OK
    ( $$ SELECT plogin.admin_toggle ( 'plogin_user_' || id , TRUE )
           FROM tap.account
          WHERE email = 'admin@example.com'
      $$
    , 'permission denied for function admin_toggle'
    , 'normal user cannot do anything'
    );


-----------------------
SELECT * FROM finish();
ROLLBACK;
