BEGIN;

SELECT plan (21);

-- Create a user to delete.
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'Deleted', 'deleted@example.com', 'passwordpass'
      $$
    , 'Create deleted user'
    );

SELECT LIVES_OK
    ( $$ SELECT plogin.role_add('plogin_user_' || id, 'plogin_perms_test', TRUE)
           FROM plogin.account
          WHERE email = 'deleted@example.com'
      $$
    , 'configure roles'
    );


--
-- account delete fails when
-- user attempts to delete self.
--

SELECT THROWS_OK
    ( $$ DELETE FROM plogin.account WHERE id = 1 $$
    , 'Cannot delete self'
    );

-- Ensure that updating fails
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET deleted_at = NOW()
        WHERE id = 1
    $$
  , 'Cannot delete self'
  );

--
-- account delete fails when
-- user doesn't have permission to delete
--
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         VALUES ( 'Test', 'test@example.com', 'passwordpass' )
      $$
    , 'Create other user'
    );

-- Set the current role to the other user.
SET ROLE none;
SELECT tap.role_set('test@example.com');

-- Ensure that deleting fails
SELECT THROWS_OK
    ( $$ DELETE FROM plogin.account
          WHERE email = 'deleted@example.com'
      $$
    , 'permission denied for table account'
    , 'user should not be able to delete account'
    );

SELECT THROWS_OK
    ( $$ UPDATE plogin.account
            SET deleted_at = NOW()
          WHERE email = 'deleted@example.com'
      $$
    , 'permission denied for table account'
    , 'user should not be able to set deleted_at'
    );

-- Ensure that the data is still correct
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT deleted_at, deleted_by, name
         FROM tap.account
        WHERE email = 'deleted@example.com'
    $$
  , $$ SELECT NULL::TIMESTAMPTZ, NULL::INT, 'Deleted' $$
  , 'data is still correct'
  );

-- Ensure the role still exists
SELECT LIVES_OK
  ( $$ SELECT tap.role_set('deleted@example.com') $$
  , 'role still exists'
  );


--
-- account delete succeeds when
-- the user has permission
--
SET ROLE none;
SELECT id INTO tap._deleted_id FROM tap.account WHERE email = 'deleted@example.com';

SET ROLE plogin_user_1;
SELECT RESULTS_EQ
  ( $$ WITH deleted AS (
           DELETE FROM plogin.account
            WHERE email = 'deleted@example.com'
           RETURNING email
       )
      SELECT COUNT(*)::INT FROM deleted
    $$
  , $$ SELECT 1 $$
  , 'deleting user works'
  );

-- Ensure that the data has been blanked
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT deleted_at, deleted_by, email, name
         FROM tap.account
        WHERE id = (SELECT * FROM tap._deleted_id)
    $$
  , $$ SELECT NOW(), 1, ((SELECT * FROM tap._deleted_id) || '@deleted')::citext, 'deleted' $$
  , 'data has been blanked'
  );

-- Ensure we can no longer set the role
SELECT THROWS_OK
  ( $A$ DO $$ BEGIN EXECUTE 'SET ROLE plogin_user_' || (SELECT * FROM tap._deleted_id); END; $$; $A$
  , 'role "plogin_user_' || (SELECT * FROM tap._deleted_id) || '" does not exist'
  );

-- Ensure that the correct audit log has been created
SELECT RESULTS_EQ
  ( $$ SELECT type, at, by, who, account
         FROM tap.account_audit
    $$
  , $$ SELECT 'deleted'::plogin.account_audit_type
            , NOW()
            , 1
            , (SELECT * FROM tap._deleted_id)
            , NULL::plogin.account_audit_user
    $$
  , 'ensure audit logs exist'
  );

-- Ensure that no further modifications can be made to the user
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET name = 'x'
        WHERE id = (SELECT * FROM tap._deleted_id)
    $$
  , 'permission denied for table account'
  );


--
-- account delete succeeds when
-- the user does it through an UPDATE
--

-- Create another user to delete.
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'Deleted', 'deleted@example.com', 'passwordpass'
      $$
    , 'Create deleted user'
    );

SET ROLE none;
DROP TABLE tap._deleted_id;
SELECT id INTO tap._deleted_id FROM tap.account WHERE email = 'deleted@example.com';

SET ROLE plogin_user_1;
SELECT RESULTS_EQ
  ( $$ WITH deleted AS (
           UPDATE plogin.account
              SET deleted_by = 1
            WHERE email = 'deleted@example.com'
            RETURNING email
       )
      SELECT COUNT(*)::INT FROM deleted
    $$
  , $$ SELECT 1 $$
  , 'deleting user through update works'
  );

-- Ensure that the data has been blanked
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT deleted_at, deleted_by, email, name
         FROM tap.account
        WHERE id = (SELECT * FROM tap._deleted_id)
    $$
  , $$ SELECT NOW(), 1, ((SELECT * FROM tap._deleted_id) || '@deleted')::citext, 'deleted' $$
  , 'data has been blanked'
  );

-- Ensure we can no longer set the role
SELECT THROWS_OK
  ( $A$ DO $$ BEGIN EXECUTE 'SET ROLE plogin_user_' || (SELECT * FROM tap._deleted_id); END; $$; $A$
  , 'role "plogin_user_' || (SELECT * FROM tap._deleted_id) || '" does not exist'
  );

-- Ensure that the correct audit log has been created
SELECT RESULTS_EQ
  ( $$ SELECT type, at, by, who, account
         FROM tap.account_audit
        ORDER BY id DESC
        LIMIT 1
    $$
  , $$ SELECT 'deleted'::plogin.account_audit_type
            , NOW()
            , 1
            , (SELECT * FROM tap._deleted_id)
            , NULL::plogin.account_audit_user
    $$
  , 'ensure audit logs exist'
  );

-- Ensure that no further modifications can be made to the user
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET name = 'x'
        WHERE id = (SELECT * FROM tap._deleted_id)
    $$
  , 'permission denied for table account'
  );

--
-- account delete does nothing when
-- the user does not exist
--
SET ROLE plogin_user_1;
SELECT RESULTS_EQ
  ( $$ WITH deleted AS (
          DELETE FROM plogin.account
           WHERE email = 'doesnotexist@example.com'
          RETURNING id
       )
       SELECT COUNT(*)::INT FROM deleted
    $$
  , $$ SELECT 0 $$
  , 'deleting non-existent user does nothing'
  );


 -----------------------
 SELECT * FROM finish();
 ROLLBACK;
