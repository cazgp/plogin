BEGIN;

SELECT plan (21);

-- Create a normal user.
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'Normal', 'normal@example.com', 'passwordpass'
      $$
    , 'Create normal user'
    );

-- Create an other user
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'Other', 'other@example.com', 'passwordpass'
      $$
    , 'Create normal user'
    );


--
-- account_update succeeds when
-- the user can edit other people.
--
SELECT LIVES_OK
  ( $$ UPDATE plogin.account
         SET name = 'Name'
       WHERE email = 'normal@example.com';
    $$
  , 'updating account to add roles'
  );


--
-- account_update fails when
-- the user cannot edit other people.
--
SET ROLE NONE;
SELECT tap.role_set('normal@example.com');
SELECT IS_EMPTY
  ( $$ UPDATE plogin.account
         SET name = 'Name'
       WHERE email = 'other@example.com'
       RETURNING name
    $$
  , 'user cannot edit other people'
  );


--
-- account_update succeeds when
-- the user can edit themselves.
--
SELECT RESULTS_EQ
  ( $$ UPDATE plogin.account
         SET name = 'yay new name'
       WHERE email = 'normal@example.com'
       RETURNING name
    $$
  , $$ SELECT 'yay new name' $$
  , 'user can edit self'
  );


--
-- account_update fails when
-- the user tries to changs their password to too short.
--
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
         SET password = 'abc'
       WHERE email = 'normal@example.com'
    $$
  , 'Password too short'
  );


--
-- account_update fails when
-- the user tries to changs their email to invalid.
--
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
         SET email = 'abc'
       WHERE email = 'normal@example.com'
    $$
  , 'new row for relation "account" violates check constraint "account_email_check"'
  );

--------
-- ADMIN
--------

--
-- account_update fails
-- when non-admin tries to set is_admin option
--
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET is_admin = TRUE
        WHERE email = 'other@example.com'
    $$
  , 'permission denied for table account'
  );

--
-- account_update fails
-- when non-admin tries to revoke is_admin option
--
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET is_admin = FALSE
        WHERE email = 'other@example.com'
    $$
  , 'permission denied for table account'
  );


--
-- account_update fails
-- when attempting to revoke admin from uber-user
--
SET ROLE plogin_user_1;
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET is_admin = FALSE
        WHERE id = 1
    $$
  , 'Cannot revoke admin from uber-user'
  );

--
-- account_update succeeds
-- when admin adds is_admin option to user
--
SELECT LIVES_OK
  ( $$ UPDATE plogin.account
          SET is_admin = TRUE
        WHERE email = 'normal@example.com'
    $$
  , 'can set normal user to admin'
  );

-- Check that the user is an admin by adding admin rights to 'other'.
SET ROLE NONE;
SELECT tap.role_set('normal@example.com');
SELECT LIVES_OK
  ( $$ UPDATE plogin.account
          SET is_admin = TRUE
        WHERE email = 'other@example.com'
    $$
  , 'can set other user to admin'
  );

-- Revoke the admin rights again.
SELECT LIVES_OK
  ( $$ UPDATE plogin.account
          SET is_admin = FALSE
        WHERE email = 'other@example.com'
    $$
  , 'can revoke admin from other user'
  );

-- Ensure they can create users.
SELECT LIVES_OK
  ( $$ INSERT INTO plogin.account (name, email, password)
       SELECT 'new', 'new@example.com', 'passpasspass'
    $$
  , 'admin user can create users'
  );

--
-- account_update fails
-- when admin attempts to revoke admin from self
--
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET is_admin = FALSE
        WHERE email = 'normal@example.com'
    $$
  , 'Cannot revoke admin from self'
  );

--
-- account_update succeeds
-- when admin revokes is_admin option
--
SET ROLE plogin_user_1;
SELECT LIVES_OK
  ( $$ UPDATE plogin.account
          SET is_admin = FALSE
        WHERE email = 'normal@example.com'
    $$
  , 'can revoke admin from normal user'
  );

-- Ensure normal user can no longer change admin rights.
SET ROLE NONE;
SELECT tap.role_set('normal@example.com');
SELECT THROWS_OK
  ( $$ UPDATE plogin.account
          SET is_admin = FALSE
        WHERE email = 'other@example.com'
    $$
  , 'permission denied for table account'
  , 'normal cannot change admin rights'
  );

-- Ensure they can no longer create users.
SELECT THROWS_OK
  ( $$ INSERT INTO plogin.account (name, email, password)
       SELECT 'new', 'new@example.com', 'passpasspass'
    $$
  , 'permission denied for table account'
  , 'normal cannot create users'
  );

--
-- account_update audits properly
--

-- Add a new role to "normal" and edit them.
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ SELECT plogin.role_add('plogin_user_' || id, 'plogin_perms_test', TRUE)
           FROM plogin.account
          WHERE email = 'normal@example.com'
      $$
    , 'add new role to normal'
    );

SELECT LIVES_OK
    ( $$ UPDATE plogin.account
            SET name = 'moose'
          WHERE email = 'normal@example.com'
      $$
    , 'change normal name'
    );

-- Retrieve the audit details.
SET ROLE NONE;
SELECT RESULTS_EQ
    ( $$ SELECT by, who, account FROM tap.account_audit ORDER BY by, who, account $$
    , $$
        SELECT 1
          , (SELECT id FROM tap.account WHERE email = 'normal@example.com')
          , UNNEST(ARRAY
            [ '(moose,{plogin_perms_test})'
            , '(Name,{})'
            , '("yay new name",{})'
            , '("yay new name",{plogin_perms_admin})'
            ])::plogin.account_audit_user
         UNION
         SELECT (SELECT id FROM tap.account WHERE email = 'normal@example.com')
              , (SELECT id FROM tap.account WHERE email = 'normal@example.com')
              , ROW('yay new name', '{}')::plogin.account_audit_user
         UNION
         SELECT (SELECT id FROM tap.account WHERE email = 'normal@example.com')
              , (SELECT id FROM tap.account WHERE email = 'other@example.com')
              , UNNEST(ARRAY[ '(Other,{})', '(Other,{plogin_perms_admin})' ])::plogin.account_audit_user
         ORDER BY 1, 2, 3
      $$
    , 'audit details should be correct'
    );


-----------------------
SELECT * FROM finish();
ROLLBACK;
