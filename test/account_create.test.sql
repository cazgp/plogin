BEGIN;

SELECT plan (9);

--
-- account creation succeeds when
-- creating a user without CREATE ROLE privileges.
--
SET ROLE plogin_user_1;
SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'name', 'normal@example.com', 'passwordpass'
      $$
    , 'Create normal user'
    );

-- Ensure that the new user cannot create new users
SET ROLE none;
SELECT tap.role_set('normal@example.com');
SELECT THROWS_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'name', 'another@example.com', 'passwordpass'
      $$
    , 'permission denied for table account');

-- Ensure the metadata fields are correct.
SET ROLE none;
SELECT RESULTS_EQ
    ( $$ SELECT blocked_at, blocked_by, created_at, created_by, deleted_at, deleted_by
           FROM tap.account
          WHERE email = 'normal@example.com'
      $$
    , $$ SELECT NULL::TIMESTAMPTZ, NULL::INT, NOW(), 1, NULL::TIMESTAMPTZ, NULL::INT $$
    , 'metadata fields are correct'
    );

-- Ensure that plogin can manage the new guy.
SET ROLE none;
SELECT IS_MEMBER_OF
    ( 'plogin_user_' || ( SELECT id FROM tap.account WHERE email = 'normal@example.com')
    , 'plogin'
    );

-- Ensure that the new user has plogin_everyone.
SELECT IS_MEMBER_OF
    ( 'plogin_everyone'
    , 'plogin_user_' || ( SELECT id FROM tap.account WHERE email = 'normal@example.com')
    );


--
-- account creation fails when
-- SQL ROLE cannot create accounts.
--
SET ROLE none;
SELECT THROWS_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         SELECT 'no', 'no', 'passwordpass'
      $$
    , 'permission denied for table account'
    );


--
-- account creation fails when
-- attempting to add an existing email address
--
SET ROLE plogin_user_1;
SELECT THROWS_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         VALUES ('name', 'normal@example.com', 'passwordpass')
      $$
    , 'duplicate key value violates unique constraint "account_email_key"');


--
-- account creation fails when
-- email address is invalid
--
SELECT THROWS_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         VALUES ('name', 'bademail', 'passwordpass')
      $$
    , 'new row for relation "account" violates check constraint "account_email_check"');


--
-- account creation fails when
-- password is too short
--
SELECT THROWS_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         VALUES ('name', 'b@example.com', 'passwordass')
      $$
    , 'Password too short');



-----------------------
SELECT * FROM finish();
ROLLBACK;
