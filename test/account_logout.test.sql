BEGIN;

SELECT plan (9);

--
-- account_logout succeeds
-- when the current user attempts to logout
--
SET ROLE plogin_user_1;

SELECT LIVES_OK
    ( $$ INSERT INTO plogin.account (name, email, password)
         VALUES ( 'Test1', 'test1@example.com', 'passwordpass' )
              , ( 'Test2', 'test2@example.com', 'passwordpass' )
      $$
    , 'Create users'
    );

-- Login as the first user.
SET ROLE plogin;
SELECT RESULTS_EQ
  ( $$ SELECT account_id FROM plogin.account_login('test1@example.com', 'passwordpass') $$
  , $$ SELECT id FROM account WHERE email = 'test1@example.com' $$
  , 'login test1 should work'
  );

-- Check the account_session table.
SET ROLE none;
SELECT RESULTS_EQ
  ( $$ SELECT COUNT(*)::INT FROM tap.account_session $$
  , $$ SELECT 1 $$
  , 'only one entry should be in account session'
  );

SELECT RESULTS_EQ
  ( $$ SELECT ac.email
         FROM tap.account_session "acs"
         JOIN tap.account "ac" ON acs.account = ac.id
    $$
  , $$ SELECT 'test1@example.com'::CITEXT $$
  , 'only one email should be in account session'
  );

-- Set the role
SELECT LIVES_OK
  ( $$ SELECT tap.role_set('test1@example.com') $$
  , 'set test1'
  );

-- Ensure logging in as the second user fails.
SELECT THROWS_OK
  ( $$ SELECT plogin.account_login('test2@example.com', 'password') $$
  , 'permission denied for function account_login'
  );

-- Ensure logging out succeeds.
SELECT LIVES_OK
  ( $$ SELECT plogin.account_logout() $$
  , 'logout of current user should succeed'
  );

-- Ensure the user has changed back.
SELECT tap.assert_current_user('plogin');

-- Ensure there are no more entries in account_session
SELECT IS_EMPTY
  ( $$ SELECT * FROM account_session $$
  , 'no account sessions should exist'
  );

-----------------------
SELECT * FROM finish();
ROLLBACK;
