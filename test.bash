#!/bin/bash -ex

# Ensure the right variables are set
VERSION=${VERSION:-16}
CONTAINER_NAME=${CONTAINER_NAME?CONTAINER_NAME must be set}
IMAGE_NAME=${IMAGE_NAME?IMAGE_NAME must be set}
PLOGIN_PASSWORD=${PLOGIN_PASSWORD?PLOGIN_PASSWORD must be set}

executable=$(command -v podman || command -v docker)

# If no arguments are provided, then set them to every file in the directory
if [ $# -eq 0 ]; then
	set -- "$@" $(ls test/*.test.sql)
fi

container_name="$CONTAINER_NAME-test"
image_name="$IMAGE_NAME-test:$VERSION"

# If we're wanting to do a build, then kill any running containers and rebuild
if [[ ! -z $DO_BUILD ]]; then
	$executable container stop "$container_name" || true
	$executable container kill "$container_name" || true
	./build.bash 1
fi

# Check to see if the container is running, and if it's not, start it
$executable container ls --filter name="^$container_name\$" | grep "$container_name" || {
	$executable run --rm -it -d \
		-p "$HOST_PORT_TEST:5432" \
		--name "$container_name" \
		--privileged \
		"$image_name"

	# Ensure the container is running
	expect <<-EOF
		  set timeout 10
		  spawn $executable logs -f "$container_name"
		  expect "database system is ready to accept connections"
	EOF

	# Ensure to install pgtap on the database
	$executable exec -i "$container_name" psql -U postgres -d postgres <test/pgtap.sql
}

$executable run -it --rm \
	--privileged \
	--volume "$(pwd):/go" \
	--workdir /go \
	-e PGPASSWORD=$PLOGIN_PASSWORD \
	-e PGOPTIONS="--search_path=tap,plogin -c client_min_messages=ERROR" \
	--net=host \
	cazgp/pg_prove -v -U plogin -d postgres -h localhost $@
