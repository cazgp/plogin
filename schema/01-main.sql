-- Create a role and schema for this extension
\set plogin_password `echo "$PLOGIN_PASSWORD"`

CREATE USER plogin WITH LOGIN NOINHERIT ENCRYPTED PASSWORD :'plogin_password';
CREATE SCHEMA AUTHORIZATION plogin;

CREATE EXTENSION citext WITH SCHEMA plogin;
CREATE EXTENSION pgcrypto WITH SCHEMA plogin;
CREATE EXTENSION "uuid-ossp" WITH SCHEMA plogin;

-- We need an "everyone" role so that permissions can be applied.
-- All roles will be added to this through account_create.
CREATE ROLE plogin_everyone;

-- This permission is required for accounts creation, as that's hard-coded.
CREATE ROLE plogin_perms_admin WITH CREATEROLE INHERIT IN ROLE plogin_everyone;

-- This is used in triggers to ensure that we record the correct user
-- information when auditing.
CREATE DOMAIN whoami AS NAME CHECK ( VALUE = CURRENT_USER );

-- Ensure that no-one can do anything unless they're given the explicit permission.
REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA plogin FROM PUBLIC;
ALTER DEFAULT PRIVILEGES REVOKE EXECUTE ON FUNCTIONS FROM PUBLIC;
ALTER DEFAULT PRIVILEGES REVOKE EXECUTE ON FUNCTIONS FROM plogin;

-- Helper function to return the currently logged in user ID
CREATE OR REPLACE FUNCTION plogin.current_account_id()
RETURNS INT
LANGUAGE SQL
AS $$
  SELECT regexp_replace(CURRENT_USER, 'plogin_user_', '')::INT
$$;

-- Table containing the bare minimum information of a user's account
CREATE TABLE plogin.account
  ( id SERIAL PRIMARY KEY
  , email plogin.citext NOT NULL UNIQUE CHECK (email ~~ '%@%')
  , name TEXT NOT NULL
  , is_admin BOOLEAN NOT NULL DEFAULT FALSE
  , password TEXT NOT NULL
  , blocked_at TIMESTAMPTZ
  , blocked_by INT REFERENCES plogin.account
  , created_at TIMESTAMPTZ
  , created_by INT REFERENCES plogin.account
  , deleted_at TIMESTAMPTZ
  , deleted_by INT REFERENCES plogin.account

  , CHECK
    (  (blocked_at IS NULL AND blocked_by IS NULL)
    OR (blocked_at IS NOT NULL AND blocked_by IS NOT NULL)
    )

  , CHECK
    (  (deleted_at IS NULL AND deleted_by IS NULL)
    -- Ensure the data has been wiped.
    -- TODO at some point maybe think about how the password should be changed.
    OR (deleted_at IS NOT NULL AND deleted_by IS NOT NULL AND name = 'deleted' AND email = id || '@deleted')
    )
  );

-- Session management
CREATE TABLE plogin.account_session
  ( id SERIAL PRIMARY KEY
  , token uuid DEFAULT plogin.uuid_generate_v4() NOT NULL
  , account INT REFERENCES plogin.account NOT NULL UNIQUE
  , expires_at TIMESTAMPTZ DEFAULT (NOW() + '01:00:00'::interval) NOT NULL
  );

------ FUNCTIONS ------

---
--- Login
-- This will create a session in the sessions table that allows a user to continue
-- to use the site. However, it does not set ROLEs or anything. That's to be done
-- elsewise.
---
CREATE OR REPLACE FUNCTION plogin.account_login(_email plogin.citext, _password TEXT)
RETURNS TABLE(account_id INT, session_token UUID)
LANGUAGE plpgsql
SECURITY DEFINER SET search_path = plogin
AS $$
DECLARE
    _account_id INT;
    _is_blocked BOOL;
    _is_deleted BOOL;
BEGIN
    -- Lookup the account first and use that to determine whether or not to run
    -- an extra crypt. If the account doesn't exist, we need to pretend it does.
    SELECT id, blocked_at IS NOT NULL, deleted_at IS NOT NULL
      INTO _account_id, _is_blocked, _is_deleted
      FROM account
     WHERE email = _email;

    -- If the account doesn't exist at all, then we need to run a crypt.
    IF _account_id IS NULL THEN
        PERFORM crypt(_password, gen_salt('bf', 8));

    -- Otherwise, we try to login.
    ELSE
        SELECT id INTO _account_id
        FROM account
        WHERE email = _email
          AND password = crypt(_password, password);
    END IF;

    -- If blocked or deleted then set the account to NULL so the user cannot login.
    IF _is_blocked OR _is_deleted THEN
        _account_id := NULL;
    END IF;

    -- Create an audit log of this login, whether or not it was successful
    -- Don't bother recording if it was successful but blocked/deleted.
    INSERT INTO login_audit (type, email)
    SELECT
          -- when the account id exists it means the login was successful
          CASE WHEN _account_id IS NOT NULL
               THEN 'succeeded'::login_audit_type
               ELSE 'failed'::login_audit_type
          END
        , _email;

    IF _account_id IS NULL THEN
        RETURN;
    END IF;

    RETURN QUERY
    INSERT INTO plogin.account_session ( account )
    SELECT _account_id
    ON CONFLICT
        ON CONSTRAINT account_session_account_key
        DO UPDATE SET expires_at = NOW() + INTERVAL '1 hour'
    RETURNING account, token;

END;
$$;


---
-- Allow logging self out.
---
CREATE OR REPLACE FUNCTION plogin.account_logout()
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  _account INT;
BEGIN
  DELETE FROM plogin.account_session
   WHERE account = plogin.current_account_id();

  SET ROLE none;
END;
$$;

---
--- Set the user's role from a token.
-- If the role setting fails because the token is invalid, force the user to wait.
---
CREATE OR REPLACE FUNCTION plogin.account_role_set(_token UUID)
RETURNS TABLE(id INT, name TEXT)
LANGUAGE plpgsql
AS $$
DECLARE
    _found_id INT;
    _found_name TEXT;
BEGIN
    -- Try to find a valid account / session token
    SELECT account.id, account.name INTO _found_id, _found_name
      FROM account
      JOIN account_session ON account.id = account_session.account
     WHERE account_session.token = _token
       AND account_session.expires_at > NOW();

    -- sleep for a second to punish the user for having the wrong session
    -- or expired token. yes, this is a problem for real users, but it also
    -- prevents spamming us with bad data.
    IF _found_id IS NULL THEN
        PERFORM pg_sleep(1);

        -- Ensure that the role is back to the default and return nothing.
        SET ROLE plogin;
        RETURN;
    END IF;

    -- If it has been found then we make sure to let them have their session token
    UPDATE account_session
       SET expires_at = NOW() + INTERVAL '1 hour'
     WHERE account_session.token = _token;

    -- Ensure that we set the role.
    EXECUTE 'SET ROLE plogin_user_' || _found_id;

    -- Return the user information
    RETURN QUERY SELECT _found_id, _found_name;
END;
$$;


---
--- Helper function to retrieve role names.
---
CREATE FUNCTION plogin.roles_get(_id INT)
RETURNS TABLE(name TEXT)
LANGUAGE plpgsql
AS $$
BEGIN
    RETURN QUERY
    SELECT rolname::TEXT "name"
      FROM pg_roles
     WHERE pg_has_role('plogin_user_' || _id, oid, 'member')
       AND rolname ILIKE 'plogin_perms_%';
END;
$$;


---
--- Helper functions to set and revoke user roles.
---

-- Admin toggle.
-- Designed to be idempotent, so callable by both INSERT and UPDATE.
-- This way the same code is always used so we have consistency.
-- Done as the uber-user so that it can GRANT in the future but still
-- be deleted without affecting said GRANTs.
CREATE FUNCTION plogin.admin_toggle ( username TEXT, admin BOOL )
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  _current_user TEXT;
  _extra TEXT;
BEGIN
    SELECT CURRENT_USER INTO _current_user;
    SET ROLE plogin_user_1;

    IF username = 'plogin_user_1' THEN
        RAISE EXCEPTION 'Cannot admin toggle uber user';
    END IF;

    IF admin THEN
        PERFORM plogin.role_add(username, 'plogin_perms_admin', TRUE);
        PERFORM plogin.role_add(username, 'plogin_everyone', TRUE);
        EXECUTE FORMAT('ALTER ROLE %s CREATEROLE', username);
    ELSE
        PERFORM plogin.role_remove(username, 'plogin_perms_admin');
        PERFORM plogin.role_add(username, 'plogin_everyone', FALSE);
        EXECUTE FORMAT('ALTER ROLE %s NOCREATEROLE', username);
    END IF;

    EXECUTE FORMAT('SET ROLE %s', _current_user);
END;
$$;

-- Role add.
-- Specifies whether or not the user has the admin option.
-- Explicitly GRANT with admin FALSE/TRUE as that allows us to toggle nicely.
CREATE FUNCTION plogin.role_add ( username TEXT, role_name TEXT, admin BOOL )
RETURNS VOID
LANGUAGE plpgsql
AS $$
DECLARE
  _extra TEXT;
BEGIN
    _extra := CASE WHEN admin THEN ', ADMIN TRUE' ELSE ', ADMIN FALSE' END;
    EXECUTE FORMAT('GRANT %s TO %s WITH INHERIT TRUE %s', role_name, username, _extra);
END;
$$;

-- Role remove.
-- Nothing fancy, just revokes the role from the user.
CREATE FUNCTION plogin.role_remove ( username TEXT, role_name TEXT )
RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
    EXECUTE FORMAT('REVOKE %s FROM %s', role_name, username);
END;
$$;


---
--- Trigger to ensure that variables going into the account table are correct.
---
CREATE FUNCTION plogin.account_trigger() RETURNS trigger
LANGUAGE plpgsql
AS $$
DECLARE
  _old_role TEXT;
  _username TEXT;
BEGIN
    SELECT current_user INTO _old_role;

    -- Hash the password if needed
    IF OLD.password IS NULL OR NEW.password != OLD.password THEN
        -- Check that the new password is of the correct length.
        -- TODO work out a password length policy.
        IF LENGTH(NEW.password) < 12 THEN
          RAISE EXCEPTION 'Password too short';
        END IF;

        NEW.password = plogin.crypt(NEW.password, plogin.gen_salt('bf', 8));
    END IF;

    -- INSERT
    IF TG_OP = 'INSERT' THEN
      _username := 'plogin_user_' || NEW.id;

      -- Ensure all fields that should not be set are not set.
      NEW.blocked_at = NULL;
      NEW.blocked_by = NULL;
      NEW.created_at = NOW();
      NEW.deleted_at = NULL;
      NEW.deleted_by = NULL;

      -- Create a role for the new account.
      SET ROLE plogin_user_1;
      EXECUTE FORMAT('CREATE ROLE %s', _username);
      PERFORM plogin.admin_toggle(_username, NEW.is_admin);
      EXECUTE FORMAT('GRANT %s TO plogin', _username);
      EXECUTE FORMAT('SET ROLE %s', _old_role);

      -- If caller does not match 'plogin_user_*', then this is the first creation.
      IF CURRENT_USER !~~ 'plogin_user_%' THEN
        NEW.created_by = NEW.id;
      ELSE
        NEW.created_by := plogin.current_account_id();
      END IF;

    -- UPDATE
    ELSIF TG_OP = 'UPDATE' THEN
      _username := 'plogin_user_' || OLD.id;

      -- If we are changing the admin settings, make sure that this is done as the uber-user.
      -- Otherwise if the calling user is ever downgraded in the future, all GRANTs they have
      -- ever done would be revoked cascading, which is definitely not what we want.
      IF OLD.is_admin != NEW.is_admin THEN
          IF NEW.is_admin THEN
              PERFORM plogin.admin_toggle(_username, TRUE);
          ELSE
              -- Ensure we are not trying to revoke admin from uber-user.
              IF NEW.id = 1 THEN
                  RAISE EXCEPTION 'Cannot revoke admin from uber-user';
              END IF;
 
              -- Ensure the user is not trying to revoke admin from themselves.
              IF NEW.id = plogin.current_account_id() THEN
                  RAISE EXCEPTION 'Cannot revoke admin from self';
              END IF;

              PERFORM plogin.admin_toggle(_username, FALSE);
          END IF;
      END IF;

      -- If we are blocking the user, make sure the correct fields are set.
      IF OLD.blocked_at IS NULL AND (
         NEW.blocked_at IS NOT NULL OR NEW.blocked_by IS NOT NULL
      ) THEN
          -- Ensure that the user is not attempting to block themselves.
          IF NEW.id = plogin.current_account_id() THEN
              RAISE EXCEPTION 'Cannot block self';
          END IF;
          NEW.blocked_at = NOW();
          NEW.blocked_by = plogin.current_account_id();

          -- Revoke perms from their account.
          EXECUTE FORMAT('REVOKE plogin_user_%s FROM plogin', OLD.id);

      END IF;

      -- If we are attempting to reblock the user, raise an error.
      IF OLD.blocked_at IS NOT NULL AND NEW.blocked_at IS NOT NULL THEN
          RAISE EXCEPTION 'Cannot block an already blocked account';
      END IF;

      -- If we are unblocking the user then we need to reinstate their account.
      IF OLD.blocked_at IS NOT NULL AND (
         NEW.blocked_at IS NULL OR NEW.blocked_by IS NULL
      ) THEN
          NEW.blocked_at = NULL;
          NEW.blocked_by = NULL;
          EXECUTE FORMAT('GRANT plogin_user_%s TO plogin', OLD.id);
      END IF;

      -- If we are deleting the user, make sure the correct fields are set,
      -- and that all the data is wiped.
      IF OLD.deleted_at IS NULL AND (
         NEW.deleted_at IS NOT NULL OR NEW.deleted_by IS NOT NULL
      ) THEN
          -- Ensure that the user is not attempting to delete themselves.
          IF NEW.id = plogin.current_account_id() THEN
              RAISE EXCEPTION 'Cannot delete self';
          END IF;
          NEW.deleted_at = NOW();
          NEW.deleted_by = plogin.current_account_id();
          NEW.email = OLD.id || '@deleted';
          NEW.name = 'deleted';

          -- Delete the role
          _username := 'plogin_user_' || OLD.id;
          EXECUTE FORMAT('DROP ROLE %s', _username);
      END IF;

    END IF;

    RETURN NEW;
END;
$$;

CREATE TRIGGER account_trigger BEFORE INSERT OR UPDATE ON plogin.account FOR EACH ROW EXECUTE PROCEDURE plogin.account_trigger();

---
-- Helper function to prevent deleting the user table.
-- This allows the user to still do DELETE FROM RETURNING syntax, but we ensure soft deletion.
---
CREATE RULE account_soft_delete AS
    ON DELETE TO plogin.account
    DO INSTEAD
    UPDATE plogin.account
       SET deleted_at = NOW()
     WHERE id = OLD.id
    RETURNING *;

---
-- Permissions
---

-- Revoke everything on this schema from plogin, so that we can selectively give permissions back.
REVOKE ALL ON SCHEMA plogin FROM plogin;

-- Allow the everyone user to see the schema.
GRANT USAGE ON SCHEMA plogin to plogin;
GRANT USAGE ON SCHEMA plogin to plogin_everyone;
GRANT EXECUTE ON FUNCTION plogin.current_account_id() TO plogin_everyone;

-------------
-- account --
-------------
ALTER TABLE plogin.account ENABLE ROW LEVEL SECURITY;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE plogin.account_id_seq TO plogin_perms_admin;
GRANT DELETE ON TABLE plogin.account TO plogin_perms_admin;
GRANT INSERT(blocked_at, blocked_by, created_at, created_by, deleted_at, deleted_by, email, id, is_admin, name, password) ON TABLE plogin.account TO plogin_perms_admin;
GRANT REFERENCES(blocked_by, deleted_by) ON TABLE plogin.account TO plogin_perms_admin;
GRANT SELECT(blocked_at, blocked_by, deleted_at, deleted_by, is_admin) ON TABLE plogin.account TO plogin_perms_admin;
GRANT UPDATE(blocked_at, blocked_by, deleted_at, deleted_by, is_admin) ON TABLE plogin.account TO plogin_perms_admin;

-- user can view and edit their own email, id, name, and password
GRANT SELECT(email, id, name, password) ON TABLE plogin.account TO plogin_everyone;
GRANT UPDATE(email, id, name, password) ON TABLE plogin.account TO plogin_everyone;

-- anyone with account editing permissions can do mostly what they like with the table, unless the user has been deleted.
CREATE POLICY delete_admin_all ON plogin.account FOR DELETE TO plogin_perms_admin USING (deleted_at IS NULL);
CREATE POLICY insert_admin_all ON plogin.account FOR INSERT TO plogin_perms_admin WITH CHECK (deleted_at IS NULL);
CREATE POLICY select_admin_all ON plogin.account FOR SELECT TO plogin_perms_admin USING (TRUE);
CREATE POLICY update_admin_all ON plogin.account FOR UPDATE TO plogin_perms_admin USING (deleted_at IS NULL) WITH CHECK (TRUE);

-- plogin_everyone can update the table if the row matches their capabilities.
CREATE POLICY select_self_all ON plogin.account FOR SELECT TO plogin_everyone USING (id = plogin.current_account_id());
CREATE POLICY update_self_all ON plogin.account FOR UPDATE TO plogin_everyone USING (id = plogin.current_account_id());

---------------------
-- account session --
---------------------
ALTER TABLE plogin.account_session ENABLE ROW LEVEL SECURITY;
GRANT DELETE ON TABLE plogin.account_session TO plogin_everyone;
GRANT SELECT(account) ON TABLE plogin.account_session TO plogin_everyone;
CREATE POLICY logout_self_delete ON plogin.account_session FOR DELETE TO plogin_everyone USING (account = plogin.current_account_id());
CREATE POLICY logout_self_select ON plogin.account_session FOR SELECT TO plogin_everyone USING (account = plogin.current_account_id());

-------------------
-- account login --
-------------------
GRANT EXECUTE ON FUNCTION plogin.account_login(_email plogin.citext, _password TEXT) TO plogin;
REVOKE EXECUTE ON FUNCTION plogin.account_login(_email plogin.citext, _password TEXT) FROM CURRENT_USER;

--- account_role_set
GRANT EXECUTE ON FUNCTION plogin.account_role_set(_token UUID) TO plogin;
REVOKE EXECUTE ON FUNCTION plogin.account_role_set(_token UUID) FROM CURRENT_USER;

--- roles management
GRANT EXECUTE ON FUNCTION plogin.admin_toggle(username TEXT, admin BOOL) TO plogin_perms_admin;
REVOKE EXECUTE ON FUNCTION plogin.admin_toggle(username TEXT, admin BOOL) FROM CURRENT_USER;
GRANT EXECUTE ON FUNCTION plogin.role_remove(username TEXT, role_name TEXT) TO plogin_perms_admin;
REVOKE EXECUTE ON FUNCTION plogin.role_remove(username TEXT, role_name TEXT) FROM CURRENT_USER;
GRANT EXECUTE ON FUNCTION plogin.role_add(username TEXT, role_name TEXT, is_admin BOOL) TO plogin_perms_admin;
REVOKE EXECUTE ON FUNCTION plogin.role_add(username TEXT, role_name TEXT, is_admin BOOL) FROM CURRENT_USER;
GRANT EXECUTE ON FUNCTION plogin.roles_get(_id INT) TO plogin_everyone;
REVOKE EXECUTE ON FUNCTION plogin.roles_get(_id INT) FROM CURRENT_USER;

--- account_logout
GRANT EXECUTE ON FUNCTION plogin.account_logout() TO plogin_everyone;
REVOKE EXECUTE ON FUNCTION plogin.account_logout() FROM CURRENT_USER;
