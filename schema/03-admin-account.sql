---
-- Create admin account.
-- We disable triggers on the account table so that we can bootstrap the application properly.
---
\set admin_email `echo "$ADMIN_EMAIL"`
\set admin_name `echo "$ADMIN_NAME"`
\set admin_password `echo "$ADMIN_PASSWORD"`

CREATE ROLE plogin_user_1 WITH CREATEROLE;
GRANT plogin_everyone TO plogin_user_1 WITH ADMIN OPTION;
GRANT plogin_perms_admin TO plogin_user_1 WITH ADMIN OPTION;
GRANT plogin_user_1 TO plogin;

SET session_replication_role = replica;
INSERT INTO plogin.account (name, email, password, is_admin)
SELECT :'admin_name', :'admin_email', :'admin_password', TRUE;
