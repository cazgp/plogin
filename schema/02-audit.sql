-- This permission is required for auditing.
-- Ensure that everything is done as this role.
CREATE ROLE plogin_auditor;

-- Type of account action
CREATE TYPE plogin.account_audit_type AS ENUM ( 'blocked', 'deleted', 'unblocked', 'updated' );

-- Row representation of user to keep track of values which have changed.
CREATE TYPE plogin.account_audit_user AS ( name TEXT, roles TEXT[] );

-- Type of login
CREATE TYPE plogin.login_audit_type AS ENUM ('succeeded', 'failed');

-- Audit log table to keep track of what's going on account-wise.
CREATE TABLE plogin.account_audit
  ( id SERIAL PRIMARY KEY
  , type plogin.account_audit_type NOT NULL
  , at TIMESTAMPTZ NOT NULL DEFAULT NOW()
  , by INT NOT NULL REFERENCES plogin.account
  , who INT NOT NULL REFERENCES plogin.account
  , account plogin.account_audit_user

  -- If blocking or deleting, none of the account information changes, so we don't need
  -- to take a snapshot of it in the audit log.
  , CHECK
    (  (( type::TEXT = 'blocked'
       OR type::TEXT = 'deleted'
       OR type::TEXT = 'unblocked'
        ) AND account IS NULL
    )
    OR ((type::TEXT = 'created' OR type::TEXT = 'updated') AND account IS NOT NULL)
    )

  );

--
-- Auditor of updates / deletes.
-- This uses two hacks.
-- The first is the type as 'plogin.account'. With strict permissions set on the
-- account table, I don't believe a record can be constructed by anyone but a super
-- user, because they won't be able to.
-- The second is the `whoami` trick which is set when this function is called, but
-- before the security invoker. It also cannot be set to anything other than the
-- current user.
--
CREATE FUNCTION plogin._account_auditor_update_or_delete
  ( OLD plogin.account
  , NEW plogin.account
  , caller whoami DEFAULT CURRENT_USER
  )
RETURNS VOID
LANGUAGE plpgsql
SECURITY DEFINER SET search_path = plogin
AS $$
DECLARE
  _audit_type plogin.account_audit_type;
BEGIN
  IF OLD.deleted_at IS NULL AND NEW.deleted_at IS NOT NULL THEN
    _audit_type = 'deleted'::plogin.account_audit_type;
  ELSE
    IF OLD.blocked_at IS NULL AND NEW.blocked_at IS NOT NULL THEN
      _audit_type := 'blocked'::plogin.account_audit_type;
    ELSIF OLD.blocked_at IS NOT NULL AND NEW.blocked_at IS NULL THEN
      _audit_type := 'blocked'::plogin.account_audit_type;
    ELSE
      _audit_type := 'updated'::plogin.account_audit_type;
    END IF;
  END IF;

  -- Insert the log into the account audit table.
  INSERT INTO plogin.account_audit (type, who, account, by)
  SELECT _audit_type
       , NEW.id
       , CASE WHEN _audit_type = 'updated'::plogin.account_audit_type
              THEN ROW
                 ( NEW.name
                 , ARRAY(SELECT * FROM plogin.roles_get(OLD.id))
                 )::plogin.account_audit_user
              ELSE NULL
         END
       , regexp_replace(caller, 'plogin_user_', '')::INT;
END;
$$;

-- TODO I'm not a fan of this, but it's the only way to get a security definer
-- with the current_user afaict.
GRANT EXECUTE ON FUNCTION plogin._account_auditor_update_or_delete
  ( OLD plogin.account
  , NEW plogin.account
  , caller whoami
  )
TO plogin_everyone;

CREATE FUNCTION plogin.account_audit_trigger() RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
  PERFORM plogin._account_auditor_update_or_delete(OLD, NEW);
  RETURN NEW;
END;
$$;

CREATE TRIGGER account_audit_trigger AFTER UPDATE ON plogin.account
FOR EACH ROW EXECUTE PROCEDURE plogin.account_audit_trigger();

-- Audit log table to keep track of what's going on login-wise.
CREATE TABLE plogin.login_audit
  ( id SERIAL PRIMARY KEY
  , type plogin.login_audit_type NOT NULL
  , email plogin.CITEXT NOT NULL
  , at TIMESTAMPTZ DEFAULT NOW() NOT NULL
  , CHECK (at = NOW())
  );
