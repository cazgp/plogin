#!/bin/bash -ex

IS_TEST=${1}
VERSION=${VERSION:-16}

# Ensure the correct env vars are set.
ADMIN_EMAIL=${ADMIN_EMAIL?ADMIN_EMAIL must be set}
ADMIN_NAME=${ADMIN_NAME?ADMIN_NAME must be set}
ADMIN_PASSWORD=${ADMIN_PASSWORD?ADMIN_PASSWORD must be set}
PLOGIN_PASSWORD=${PLOGIN_PASSWORD?PLOGIN_PASSWORD must be set}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD?POSTGRES_PASSWORD must be set}

CONTAINER_NAME=${CONTAINER_NAME?CONTAINER_NAME must be set}
IMAGE_NAME=${IMAGE_NAME?IMAGE_NAME must be set}

if [[ ${#ADMIN_PASSWORD} -lt 12 ]]; then
	echo Admin password too short
	exit 1
fi

executable=$(command -v podman || command -v docker)

if [[ -z $IS_TEST ]]; then
	base_image="postgres:$VERSION"
	container_name="$CONTAINER_NAME"
	image_name="$IMAGE_NAME"
else
	base_image="cazgp/pgtap:$VERSION"
	container_name="$CONTAINER_NAME-test"
	image_name="$IMAGE_NAME-test"
fi

# Ensure the things don't already exist.
$executable container rm -f "$container_name"-base || true
$executable container rm -f "$container_name" || true
$executable image rm "$image_name" || true

# Launch the container in detached mode.
$executable run \
	--name "$container_name"-base \
	-d \
	--privileged \
	--volume "$(pwd)/schema/:/docker-entrypoint-initdb.d" \
	-e ADMIN_EMAIL="$ADMIN_EMAIL" \
	-e ADMIN_NAME="$ADMIN_NAME" \
	-e ADMIN_PASSWORD="$ADMIN_PASSWORD" \
	-e PLOGIN_PASSWORD="$PLOGIN_PASSWORD" \
	-e POSTGRES_PASSWORD="$POSTGRES_PASSWORD" \
	-e PGDATA=/data \
	"$base_image"

# Then attach the log reader to it.
expect <<-EOF

	set timeout 10
	spawn $executable logs -f $container_name-base

	expect "PostgreSQL init process complete; ready for start up."
	expect "database system is ready to accept connections"

EOF

# Once it's built, tear it down.
$executable commit "$container_name"-base cazgp/"$image_name":"$VERSION"
$executable container kill "$container_name"-base
